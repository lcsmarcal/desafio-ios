//
//  RepositoriesApiIntegration.swift
//  BraTeste
//
//  Created by Lucas Marcal on 23/10/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import RxSwift
import Alamofire
import SwiftyJSON

class RepositoriesApiService: RepositoriesApiProtocol{
    func fetchRepositories(page: Int) -> Observable<[RepositoryModel]> {
        return Observable.create({ observer in
            
            let response = Alamofire.request("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)")
                .responseJSON(completionHandler: { response in
                    switch response.result{
                    case .success(let value):
                        var repositories: [RepositoryModel] = []
                        
                        let jsonValue = JSON(value)
                        if let itemsJSON = jsonValue["items"].array{
                            for item in itemsJSON{
                                repositories.append(RepositoryModel(repository: item))
                            }
                        }
                        
                        observer.onNext(repositories)
                        observer.onCompleted()
                        break
                    case .failure(let error):
                        observer.onError(error)
                        break
                    }
                })
            return Disposables.create {
                response.cancel()
            }
        })
    }
    
    func fetchPullRequests(repository: RepositoryModel) -> Observable<[PullRequestModel]>{
        return Observable.create({ observer in
            
            let response = Alamofire.request("https://api.github.com/repos/\(repository.authorName)/\(repository.name)/pulls")
                .responseJSON(completionHandler: { response in
                    switch response.result{
                    case .success(let value):
                        var pullRequests: [PullRequestModel] = []
                        
                        let jsonValue = JSON(value)
                        if let itemsJSON = jsonValue.array{
                            for item in itemsJSON{
                                pullRequests.append(PullRequestModel(pullRequest: item))
                            }
                        }
                        
                        observer.onNext(pullRequests)
                        observer.onCompleted()
                        break
                    case .failure(let error):
                        break
                    }
                })
            return Disposables.create{
                response.cancel()
            }
        })
    }
}
