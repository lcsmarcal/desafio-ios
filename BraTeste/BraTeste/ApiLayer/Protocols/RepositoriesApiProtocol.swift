//
//  RepositoriesApiProtocol.swift
//  BraTeste
//
//  Created by Lucas Marcal on 23/10/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import RxSwift
import RxCocoa

protocol RepositoriesApiProtocol {
    func fetchRepositories(page: Int) -> Observable<[RepositoryModel]>
    func fetchPullRequests(repository: RepositoryModel) -> Observable<[PullRequestModel]>
}
