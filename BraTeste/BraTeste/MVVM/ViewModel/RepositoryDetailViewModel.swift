//
//  RepositoryDetailViewModel.swift
//  BraTeste
//
//  Created by Lucas Marcal on 23/10/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import RxSwift

class RepositoryDetailViewModel {
    var repository: RepositoryModel!
    var pullRequests:[PullRequestModel] = []
    
    func fetchPullRequests() -> Observable<[PullRequestModel]>{
        return DataService.sharedInstance.repositoryApiService.fetchPullRequests(repository: repository).map{
            [unowned self] pullrequests in
            self.pullRequests = pullrequests
            
            return pullrequests
        }
    }
}
