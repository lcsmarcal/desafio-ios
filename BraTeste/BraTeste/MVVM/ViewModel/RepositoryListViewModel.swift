//
//  RepositoryListViewModel.swift
//  BraTeste
//
//  Created by Lucas Marcal on 23/10/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import RxCocoa
import RxSwift

class RepositoryListViewModel{
    var repositories:[RepositoryModel] = []
    var pageNumber = 0
    var selectedRepository:RepositoryModel!
    
    func fetchRepositories() -> Observable<[RepositoryModel]>{
        pageNumber += 1
        
        return DataService.sharedInstance.repositoryApiService.fetchRepositories(page: pageNumber).map({[unowned self] repositories in
            self.repositories.append(contentsOf: repositories)
            return repositories
        })
    }
}
