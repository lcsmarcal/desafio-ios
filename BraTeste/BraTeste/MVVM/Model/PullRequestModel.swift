//
//  PullRequestModel.swift
//  BraTeste
//
//  Created by Lucas Marcal on 23/10/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import UIKit

class PullRequestModel{
    var authorName: String = ""
    var authorThumb: String = ""
    var title: String = ""
    var date: String = ""
    var body: String = ""
    var pullUrl: String = ""
    
    init(){}
}
