//
//  PullRequestJsonParser.swift
//  BraTeste
//
//  Created by Lucas Marcal on 23/10/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import SwiftyJSON

extension PullRequestModel{
    convenience init(pullRequest: JSON) {
        self.init()
        
        if let title = pullRequest["title"].string{
            self.title = title
        }
        
        if let body = pullRequest["body"].string{
            self.body = body
        }
        
        if let dateJson = pullRequest["created_at"].string{
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            if let dateConverted = dateFormatter.date(from:dateJson){
                dateFormatter.dateFormat = "dd/MM/yyyy"
                let dateString = dateFormatter.string(from: dateConverted)
                self.date = dateString
            }
        }
        
        if let url = pullRequest["html_url"].string{
            self.pullUrl = url
        }
        
        let owner = pullRequest["user"]
        
        if let authorName = owner["login"].string{
            self.authorName = authorName
        }
        if let authorThumb = owner["avatar_url"].string{
            self.authorThumb = authorThumb
        }
    }
}
