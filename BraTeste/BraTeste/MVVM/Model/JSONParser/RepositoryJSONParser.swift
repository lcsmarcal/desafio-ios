//
//  RepositoryJSONParser.swift
//  BraTeste
//
//  Created by Lucas Marcal on 23/10/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import SwiftyJSON

extension RepositoryModel{
    convenience init(repository: JSON) {
        self.init()
        
        if let id = repository["id"].int{
            self.id = id
        }
        if let name = repository["name"].string{
            self.name = name
        }
        if let desc = repository["description"].string{
            self.desc = desc
        }
        if let starCount = repository["stargazers_count"].int{
            self.starsCount = starCount
        }
        if let forkCount = repository["forks"].int{
            self.forksCount = forkCount
        }
        
        let owner = repository["owner"]
        
        if let authorName = owner["login"].string{
            self.authorName = authorName
        }
        if let authorThumb = owner["avatar_url"].string{
            self.authorThumb = authorThumb
        }
        
    }
}
