//
//  RepositoryModel.swift
//  BraTeste
//
//  Created by Lucas Marcal on 23/10/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

class RepositoryModel{
    var id: Int = 0
    var name:String = ""
    var desc:String = ""
    var authorName:String = ""
    var authorThumb: String = ""
    var starsCount:Int = 0
    var forksCount:Int = 0
    
    init(){}
}
