//
//  RepositoryDetailViewController.swift
//  BraTeste
//
//  Created by Lucas Marcal on 23/10/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import UIKit
import RxSwift
import SDWebImage
class RepositoryDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.dataSource = self
            tableView.delegate = self
            tableView.estimatedRowHeight = 201
            tableView.rowHeight = UITableViewAutomaticDimension
        }
    }
    @IBOutlet weak var repositoryLabel: UILabel!
    
    let viewModel = RepositoryDetailViewModel()
    var disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        repositoryLabel.text = viewModel.repository.name
        title = "Pull Requests"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func fetchData(){
        viewModel.fetchPullRequests()
            .subscribe(onNext:{[unowned self] _ in
                self.tableView.reloadData()
                },onError:{ [unowned self] error in
                    let alertController = UIAlertController(title: "Ocorreu um erro", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                        (result : UIAlertAction) -> Void in
                    }
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
            }).disposed(by: disposeBag)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RepositoryDetailViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let requestUrl = URL(string: viewModel.pullRequests[indexPath.row].pullUrl) {
            UIApplication.shared.openURL(requestUrl)
        }
    }
}

extension RepositoryDetailViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pullRequestCell", for: indexPath) as! PullRequestTableViewCell
        let pullRequest = viewModel.pullRequests[indexPath.row]
        
        cell.titleLabel.text = pullRequest.title
        cell.bodyLabel.text = pullRequest.body
        cell.authorLabel.text = pullRequest.authorName
        cell.createdAtLabel.text = pullRequest.date
        cell.authorThumbImageView.sd_setImage(with: URL.init(string: pullRequest.authorThumb)!, placeholderImage: #imageLiteral(resourceName: "user"))
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.pullRequests.count
    }
}
