//
//  RepositoryListViewController.swift
//  BraTeste
//
//  Created by Lucas Marcal on 23/10/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SDWebImage

class RepositoryListViewController: UIViewController {
    
    static let startLoadingOffset: CGFloat = 0.0
    static func isNearTheBottomEdge(contentOffset: CGPoint, _ tableView: UITableView) -> Bool {
        return contentOffset.y + tableView.frame.size.height + startLoadingOffset > tableView.contentSize.height
    }
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var tableview: UITableView!{
        didSet{
            tableview.dataSource = self
            
            tableview.estimatedRowHeight = 201
            tableview.rowHeight = UITableViewAutomaticDimension
        }
    }
    
    let viewModel = RepositoryListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindView()
        // Do any additional setup after loading the view.
    }
    
    func bindView(){
        let loadNextPageTrigger = tableview.rx.contentOffset
            .flatMap { [unowned self] offset in
                RepositoryListViewController.isNearTheBottomEdge(contentOffset: offset, self.tableview)
                    ? Observable.just(())
                    : Observable.empty()
        }
        
        loadNextPageTrigger.debounce(0.5, scheduler: MainScheduler.instance).subscribe(onNext:{[unowned self] value in
            self.loadData()
        }).disposed(by: disposeBag)
        
        tableview.rx.contentOffset
            .subscribe { _ in
            }
            .disposed(by:disposeBag)
        tableview.rx.itemSelected
            .subscribe(onNext:{[unowned self] indexPath in
                self.tableview.deselectRow(at: indexPath, animated: true)
                self.viewModel.selectedRepository = self.viewModel.repositories[indexPath.row]
                self.performSegue(withIdentifier: "repositoryDetailSegue", sender: self)
            }).disposed(by:disposeBag)
        
        tableview.rx.setDelegate(self)
            .disposed(by:disposeBag)
        
    }
    
    func loadData(){
        viewModel.fetchRepositories()
            .asObservable()
            .subscribe(
                onNext:{[unowned self] _ in
                    self.tableview.reloadData()
                },
                onError:{ [unowned self] error in
                    let alertController = UIAlertController(title: "Ocorreu um erro", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                        (result : UIAlertAction) -> Void in
                    }
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
            }
            ).disposed(by:disposeBag)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "repositoryDetailSegue"{
            let vc = segue.destination as! RepositoryDetailViewController
            vc.viewModel.repository = viewModel.selectedRepository
        }
    }
}

extension RepositoryListViewController: UITableViewDelegate{}
extension RepositoryListViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "repositoryCell")! as! RepositoryTableViewCell
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        
        let repository = viewModel.repositories[indexPath.row]
        
        cell.titleLabel.text = repository.name
        cell.descriptionLabel.text = repository.desc
        cell.authorNameLabel.text = repository.authorName
        cell.authorThumbImageView.sd_setImage(with: URL.init(string: repository.authorThumb)!, placeholderImage: #imageLiteral(resourceName: "user"))
        cell.forkCountLabel.text = "\(repository.forksCount)"
        cell.starCountLabel.text = "\(repository.starsCount)"
        
        return cell
    }
    
    
}
